import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import VueRouter from 'vue-router';
import axios from 'axios';

import Vue from 'vue';
import router from './router/router';
import App from './App';

Vue.config.productionTip = false;

axios.defaults.baseURL = 'https://api-dot-rare-disk-199520.appspot.com/';
axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';

Vue.use(VueRouter);

Vue.use(Vuetify, {
  theme: {
    btn_color: '#B33729',
  },
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
