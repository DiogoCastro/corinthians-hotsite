import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://api-dot-rare-disk-199520.appspot.com/'
})

axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8'
//axios.defaults.headers.common['Authorization'] = 'Bearer' + localStorage.getItem('token')

export default instance