/* eslint-disable */
import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/components/home/index'
import Cadastro from '@/components/cadastro/cadastro'

Vue.use(VueRouter)

const routes = [
  {
    path: '/cadastro',
    name: 'Cadastro',
    component: Cadastro
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  }
];
const router = new VueRouter({ mode: 'history', routes });
export default router;
